import 'package:flutter/material.dart';

import 'package:ku_contact_app/view/school_view.dart';


//https://codeburst.io/top-10-array-utility-methods-you-should-know-dart-feb2648ee3a2 list er jinish
void main ()=> runApp (new Marvels());

class Marvels extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return new MaterialApp(
      title : 'Ku Contact App',
      debugShowCheckedModeBanner: false,
      theme:  new ThemeData(

        primarySwatch: Colors.blueGrey,
        platform: TargetPlatform.android,
      ),
      home: SchoolView(),
    );
  }

}