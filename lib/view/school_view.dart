import 'package:flutter/material.dart';

import 'package:ku_contact_app/model/school_model.dart';

import 'package:ku_contact_app/view/department_view.dart';
import 'package:url_launcher/url_launcher.dart';

// import '../model/school_model.dart';


class SchoolView extends StatelessWidget {

  final List<SchoolModel>schoolList = [
    SchoolModel(name: 'SET School',department:'CSE'),
    SchoolModel(name: 'LS School',department: 'BGE'),
 SchoolModel(name: 'LS School',department: 'BGE'),
 SchoolModel(name: 'LS School',department: 'BGE'),
 SchoolModel(name: 'LS School',department: 'BGE'),
 SchoolModel(name: 'LS School',department: 'BGE'),
 SchoolModel(name: 'LS School',department: 'BGE'),
  

  ];

  @override
  Widget build(BuildContext context) {



    return Scaffold(
        backgroundColor: Theme
            .of(context)
            .primaryColor,
        appBar: AppBar(
          elevation: .5,
          title: Text('Ku Contact App'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                print("hello search");
              },
            )
          ],
        ),
        drawer: Drawer(),
        body: ListView.builder(
            itemCount: schoolList.length,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: (){
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context)=>DepartmentView(schoolModel:schoolList[index],)
                    )
                  );
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Card(
                    margin: EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 3),
                    elevation: 4,
                    child: ListTile(
                      leading: CircleAvatar(
                        child: Text(
                          schoolList[index].name.substring(0,1),
                              style: TextStyle(
                            fontWeight: FontWeight.bold,
                                fontSize: 20
                        ),
                        ),
                      ),
                      title: Text(schoolList[index].name),
                    )
                  ),
                ),
              );
            }));
  }

  launchCall(number) async {
    var url = 'tel:$number';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }




}