import 'package:flutter/material.dart';

import 'package:ku_contact_app/model/contact_model.dart';
import 'package:ku_contact_app/model/department_model.dart';
import 'package:url_launcher/url_launcher.dart';
import 'detail.dart';
import '../model/contact_model.dart';

class ContactView extends StatefulWidget {

  final DepartmentModel departmentModel;
  ContactView({this.departmentModel});

  @override
  _ContactViewState createState() => _ContactViewState();
}

class _ContactViewState extends State<ContactView> {

   List<ContactModel> finalContactList = new List<ContactModel>();

  @override
  void initState() {
 
    super.initState();
    print(widget.departmentModel.name);
    finalContactList = contactList.where((data) => data.department == widget.departmentModel.name).toList();
  }
  final List<ContactModel>contactList = [
    ContactModel(name: 'Saira Tasmin Rabita',department: 'CSE',designation: 'Assistant Programmer',email: 'rabita@ku.as.bd',phonenumber: '01711966276'),
    ContactModel(name: 'Atish Kumar',department: 'ICT Cell',designation: 'Assistant Programmer',email: 'atish@ku.as.bd',phonenumber: '01669013123'),
    ContactModel(name: 'Saira Tasmin Rabita',department: 'CSE Cell',designation: 'Assistant Programmer',email: 'rabita@ku.as.bd',phonenumber: '01711966276'),
    ContactModel(name: 'Saira Tasmin Rabita',department: 'IT Cell',designation: 'Assistant Programmer',email: 'rabita@ku.as.bd',phonenumber: '01711966276'),
    ContactModel(name: 'Saira Tasmin Rabita',department: 'ICT Cell',designation: 'Assistant Programmer',email: 'rabita@ku.as.bd',phonenumber: '01711966276'),
    ContactModel(name: 'Saira Tasmin Rabita',department: 'ICT Cell',designation: 'Assistant Programmer',email: 'rabita@ku.as.bd',phonenumber: '01711966276'),
    ContactModel(name: 'Saira Tasmin Rabita',department: 'ICT Cell',designation: 'Assistant Programmer',email: 'rabita@ku.as.bd',phonenumber: '01711966276'),
    ContactModel(name: 'Saira Tasmin Rabita',department: 'ICT Cell',designation: 'Assistant Programmer',email: 'rabita@ku.as.bd',phonenumber: '01711966276'),
    ContactModel(name: 'Saira Tasmin Rabita',department: 'ICT Cell',designation: 'Assistant Programmer',email: 'rabita@ku.as.bd',phonenumber: '01711966276'),
    ContactModel(name: 'Saira Tasmin Rabita',department: 'ICT Cell',designation: 'Assistant Programmer',email: 'rabita@ku.as.bd',phonenumber: '01711966276'),
    ContactModel(name: 'Saira Tasmin Rabita',department: 'ICT Cell',designation: 'Assistant Programmer',email: 'rabita@ku.as.bd',phonenumber: '01711966276'),
    ContactModel(name: 'Saira Tasmin Rabita',department: 'ICT Cell',designation: 'Assistant Programmer',email: 'rabita@ku.as.bd',phonenumber: '01711966276'),

  ];

  @override
  Widget build(BuildContext context) {
  


    return Scaffold(
        backgroundColor: Theme
            .of(context)
            .primaryColor,
        appBar: AppBar(
          elevation: .5,
          title: Text('Ku Contact App'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                print("hello search");
              },
            )
          ],
        ),

        body: Column(
          children: <Widget>[
            TextField(

              onChanged: (value){
                finalContactList = contactList.where(
                        (item) => item.name.toLowerCase().startsWith(value.toLowerCase())
                ).toList();
                setState(() {

                });
              },
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: finalContactList.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) =>
                                    Detail(index: index,
                                      marvel: finalContactList[index],)
                            )
                        );
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Card(
                            margin: EdgeInsets.only(
                                left: 5, right: 5, top: 2, bottom: 3),
                            elevation: 4,
                            child: ListTile(
                              leading: CircleAvatar(
                                child: Text(
                                  finalContactList[index].name.substring(0, 1),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20
                                  ),
                                ),
                              ),
                              title: Text(finalContactList[index].name),
                              subtitle: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(finalContactList[index].email),
                                  Text(finalContactList[index].phonenumber)
                                ],
                              ),
                              trailing: IconButton(
                                icon: Icon(Icons.call, color: Colors.green,),
                                onPressed: () {
                                  launchCall(
                                      finalContactList[index].phonenumber);
                                },
                              ),
                            )
                        ),
                      ),
                    );
                  })
              ,
            )
          ],
        ));
  }

  launchCall(number) async {
    var url = 'tel:$number';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}